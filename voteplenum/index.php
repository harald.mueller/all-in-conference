<html>
	<head>
	<title>Vote Plenum</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="../voteumgebung/images/aic-logo.ico" />
    <link rel="apple-touch-icon" href="../voteumgebung/images/aic-logo-touch.jpg" />
	<script type="text/javascript" src="../voteumgebung/jquery/jquery-1.6.3.min.js"></script>
  <style>

			body     {
				text-decoration: none;
			  font-family: Arial, Helvetica, Verdana, sans-serif;
			  font-size: 16pt;
			  font-style: normal;
				padding: 0px;
				color: #0476BC;
			}
			
			a:focus {
			  text-decoration: underline;
			  color: #0000CC;
			}
			a:active {
			  text-decoration: none;
			  color: #0000CC;
			}
			a:hover {
			  text-decoration: underline;
			  color: #0000CC;
			}
			a:link {
			  text-decoration: none;
			  color: #0000CC;
			}
			a:visited {
			  text-decoration: none;
			  color: #0000CC;
			}
			
			table {
				border-style:solid;
				border-width:0px;
				empty-cells:hide;
				border-color:#009DE0;
			}
			
			th {
				height: 12px;
				font-size: 9pt;
				border-color:#009DE0;
			}
			tr {
				height: 12px;
				font-size: 9pt;
			}
			td {
				border-style:none;
				height: 30px;
				font-size: 18pt;
			}
			
			p {
				 padding-left:6px;
				 padding-right:4px;
				}
			
			
			h1 	{
				 font-weight: bold;
				 padding-left: 20px;
				 font-size: 14pt;
				 color: #E0ECF8;
				 background-color: #009DE0;
			}
			
  </style>

</head>
<body bgcolor="#FFFFFF">
<form name="frm_Eingabe" method="post" >
    <table width="100%">
      <tr>
        <td valign="top"><img src="../voteumgebung/images/aic-logo131.png" border="0" />&nbsp;</td>
        <td valign="top" align="right"><!--?= $_SERVER['SERVER_ADDR'] ?--></td>
      </tr>
    </table>
<?php
include("../voteumgebung/functions.php");
$password = "";
$pwTrue = false;
if ( isset($_REQUEST['password'])  ) {
  $password = $_REQUEST['password'];
  if ($password == "Oligopol2000") {
    $pwTrue = true;
  }
}
ConnectDB();
$sqlSelect = "SELECT * FROM `".$_SESSION["db_name"]."`.`t_einstellungen`
              WHERE `was` = 'plenum_hat_aktiv';";
$result = mysql_query($sqlSelect);
if (!$result) {
  echo "DB-SELECT-Anfrage ist schief gegangen<br>". mysql_error();
  exit;
}
if (mysql_num_rows($result) == 0) {
  echo "<br>Keine Eintraege";
  exit;
}
$sichtbar = "";
while ($row = mysql_fetch_assoc($result)) {
  if ($row['wert'] == 'sichtbar_abstimmungsfragen'){
    $sichtbar = "plenum_mit_abstimmungsfragen.php";
  } else {
    $sichtbar = "plenum_mit_plenumfragen.php";
  }

}
?>
<?php if ($pwTrue == false) { ?>

    <h1>Plenum</h1>
    <p style="font-size: 24pt;">
    Passwort:
    <input type="password" name="password" id="password" style="font-size: 24pt;" value="<?= $password ?>"/>
    <input type="submit" name="submit" id="submit" style="font-size: 24pt;" value=" OK " />
    </p>
    <script type="text/javascript">
        document.getElementById("password").focus();
    </script>
<?php } else { ?>
    <input type="hidden" name="password" id="password" value="<?= $password ?>"/>

<iframe src="<?= $sichtbar ?>" name="Bildframe" width="100%" height="80%" align="left"
        scrolling="yes" marginheight="0" marginwidth="0" frameborder="0">
  <p>Ihr Browser kann leider keine eingebetteten Frames anzeigen.
     Waehle stattdessen <a href="<?= $sichtbar ?>" ><?= $sichtbar ?></a>.
  </p>
</iframe>
<script type="text/javascript">

function weiter() {
  document.frm_Eingabe.submit();
}
window.onload = function() {
  var sekunden = 15;
  window.setTimeout(weiter, sekunden * 1000);
}
</script>
<?php } ?>
</form>
</body>
</html>
