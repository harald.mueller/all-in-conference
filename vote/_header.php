<?php
include("../voteumgebung/sessions.php");
include("../voteumgebung/functions.php");
?>
<!DOCTYPE html>
<html>
	<head>
	<title>Aktuelle Frage..</title>
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="pragma" content="no-cache">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="../voteumgebung/images/aic-logo.ico" />
    <link rel="apple-touch-icon" href="../voteumgebung/images/aic-logo-touch.jpg" />
    <link rel="stylesheet" href="../voteumgebung/jquery/mobile/jquery.mobile-1.0b3.min.css" />
	<script type="text/javascript" src="../voteumgebung/jquery/jquery-1.6.3.min.js"></script>
	<script type="text/javascript" src="../voteumgebung/jquery/mobile/jquery.mobile-1.0b3.min.js"></script>
</head>
<body>
<div data-role="page">

	<div data-role="header" data-theme="b">
        <a href="http://<?= $_SERVER["HTTP_HOST"] ?>/vote/index.php?PHPSESSID=<?= session_id() ?>" data-role="button" data-icon="home" data-iconpos="notext" data-transition="fade" data-direction="reverse" class="ui-btn-left jqm-home">Menu</a>
		<h1>aic | vote</h1>
	</div><!-- /header -->

	<div data-role="content" data-theme="e">
