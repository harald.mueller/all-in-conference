<?php
session_cache_limiter('no-cache');
session_start();
$timeOutTime = 2*60*60; //in sek
$sessDiff = time() - intval($_SESSION['session_startzeit']); //Zeit zwischen jetzt und vorher
if ( $sessDiff > $timeOutTime ) {
?>
    <script language="JavaScript" type="text/javascript">
        //alert("Session ist Out");
        location.href="index.php";
    </script>
<?php
}
$_SESSION['session_startzeit'] = time();
?>
