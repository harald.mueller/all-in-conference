<?php
include("../voteumgebung/sessions.php");
//$brauchtNeueSession = true;
//if (isset($_GET['PHPSESSID'])) {
//  session_id($_GET['PHPSESSID']);
//  $brauchtNeueSession = false;
//}
$txtEmail = "Your E-Mail address:";
$error_style="border-style: solid; border-width: 2px;border-color: red;";
$error_feld_style_email="";
$email= "";
if (isset($_REQUEST['email'])){
    $email = htmlspecialchars(strip_tags( stripslashes($_REQUEST['email'])));
}
/*
if ($brauchtNeueSession) {
    //session_destroy ();
    session_unset();
    $_SESSION=array();
    session_regenerate_id(true);
    session_cache_limiter('no-cache');
    session_start();
    $_SESSION['session_startzeit'] = time();
}
if (isset($_SESSION['email'])) {
    $email = $_SESSION['email'];
}
if (strlen($email) < 4 && isset($_REQUEST['submit'])) {
    $error_feld_style_email=$error_style;
}
$posAtChar = stripos($email,"@");
if ($posAtChar < 1 && isset($_REQUEST['submit'])) {
    $error_feld_style_email=$error_style;
}
if (stripos($email,".") < 1 && isset($_REQUEST['submit'])) {
    $error_feld_style_email=$error_style;
}
if ($error_feld_style_email=="" && isset($_REQUEST['submit'])) {
    $_SESSION['email'] = $email;
    $forward = "http://". $_SERVER["HTTP_HOST"] ."/vote/menu.php?PHPSESSID=". session_id();
    //    header('Location: ' . $forward );
}
*/
?>
<!DOCTYPE html>
<html>
	<head>
	<title>aic | vote</title>
    <link rel="shortcut icon" href="../voteumgebung/images/aic-logo.ico" />
    <link rel="apple-touch-icon" href="../voteumgebung/images/aic-logo-touch.jpg" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
<!--
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="pragma" content="no-cache">
    <link rel="stylesheet"        href="../voteumgebung/jquery/mobile/jquery.mobile-1.0b3.min.css" />
	<script type="text/javascript" src="../voteumgebung/jquery/mobile/jquery.mobile-1.0b3.min.js"></script>
	<script type="text/javascript" src="../voteumgebung/jquery/ui/js/jquery-1.6.2.min.js"></script>
	<script type="text/javascript" src="../voteumgebung/jquery/ui/js/jquery-ui-1.8.16.custom.min.js"></script>
-->
    <link rel="stylesheet" href="../voteumgebung/jquery/mobile/jquery.mobile-1.0b3.min.css" />
	<script type="text/javascript" src="../voteumgebung/jquery/jquery-1.6.3.min.js"></script>
	<script type="text/javascript" src="../voteumgebung/jquery/mobile/jquery.mobile-1.0b3.min.js"></script>

</head>
<body>

<div data-role="page">

	<div data-role="header" data-theme="b">
		<h1>all in conference</h1>
	</div><!-- /header -->

	<div data-role="content" data-theme="d" >
        <center><img src='../voteumgebung/images/aic-logo.jpg' width="250" border="0"></center>
        <form name="frm_Eingabe" action="menu.php" method="post">
                <label for="email"><?= $txtEmail ?></label>
                <input type="text"
                       name="email"
                       id="email"
                       style="background-color: #FFFFFF;<?= $error_feld_style_email ?>"
                       value="<?= $email ?>"/>
                <br>
                <input type="submit"
                       name="submit"
                       id="submit"
                       data-role="button"
                       data-iconpos="right"
                       data-icon="arrow-r"
                       data-theme="e"
                       data-transition="fade"
                       value="p l e a s e &nbsp; V o t e" />
                <br>
            <!--<a href="http://<?= $_SERVER["HTTP_HOST"] ?>/vote/menu.php?PHPSESSID=<?= session_id() ?>" data-role="button" data-iconpos="right" data-icon="arrow-r" data-theme="e" >p l e a s e &nbsp; V o t e</a>-->
        </form>

	</div><!-- /content -->

	<div data-role="footer" data-theme="a">
        &nbsp;powered by Oligopol GmbH, Lachen SZ
        <center>
          <a href="tel:+41 79 757 77 57" data-theme="e">Phone</a>
        | <a href="mailto:info@oligopol.ch" data-theme="a">E-Mail</a>
        </center>
	</div><!-- /footer -->
</div><!-- /page -->
</body>
</html>
