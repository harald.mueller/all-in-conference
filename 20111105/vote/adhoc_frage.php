<?php
$txtFragestellen= "Frage stellen..";
$txtTitel = $txtFragestellen;
$txtDankeFuerFrage = "Danke fuer Ihren Beitrag";
$txtName  = "Name";
$txtFrage = "Frage";
$txtSend  = "Senden";
$txtBack = "Zurueck";
session_start();
$anzahl_min = 12*60;
if ( (time()-$_SESSION['session_startzeit']) > ($anzahl_min*60) ) {
?>
<script language="JavaScript" type="text/javascript">
    var href = window.location.href;
    alert("Sorry. Session timeout!");
      location.href="index.php";
    }
</script>
<?php
} else {
    $_SESSION['session_startzeit'] = time();
}


include("../voteumgebung/functions.php");
ConnectDB();
$error_style="border-style: solid; border-width: 2px;border-color: red;";
$error_feld_style_email="";
$error_feld_style_frage="";

$ok = false;

$fragender_name = "";
if (isset($_REQUEST['fragender_name'])){
    $fragender_name = htmlspecialchars(strip_tags( stripslashes($_REQUEST['fragender_name'])));
}
if (strlen($fragender_name)<4 && isset($_REQUEST['submit'])) {
    $error_feld_style_name=$error_style;
}



$frage = "";
if (isset($_REQUEST['frage'])){
    $frage = (htmlspecialchars(strip_tags( stripslashes($_REQUEST['frage']))));
}
if ( strlen($frage)<4 ) {
    $error_feld_style_frage=$error_style;
}

$noErr = $error_feld_style_frage.$error_feld_style_email.$error_feld_style_name;

if (strlen($noErr)<1){
    $ok=true;
            $sqlInsert = "INSERT INTO  `".$_SESSION["db_name"]."`.`adhoc_fragen` (
            `timestamp` ,
            `fragender_session` ,
            `fragender_name` ,
            `fragender_email` ,
            `frage`
            )
            VALUES (
            NULL ,
            '". session_id() ."',
            '". utf8_decode($fragender_name) ."',
            '". utf8_decode($email) ."',
            '". utf8_decode($frage) ."'
            );";
            $result = mysql_query($sqlInsert);
            if (mysql_errno()==0){
              $ok = true;
              $txtTitel = $txtDankeFuerFrage;
            } else {
              //echo mysql_errno()."<br>";
              $ok = false;
            }
}
?>

<html>
	<head>
	<title>aic | vote</title>
	<meta charset=ISO-8859-1">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="pragma" content="no-cache">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../voteumgebung/jquery/mobile/jquery.mobile-1.0b3.min.css" />
	<script type="text/javascript" src="../voteumgebung/jquery/jquery-1.6.3.min.js"></script>
	<script type="text/javascript" src="../voteumgebung/jquery/mobile/jquery.mobile-1.0b3.min.js"></script>
</head>
<body>
<form name="frm_Eingabe" action="http://<?= $_SERVER["HTTP_HOST"].$_SERVER["PHP_SELF"] ?>?PHPSESSID=<?= session_id() ?>" method="post">
   <input type="text" name="email" name="email" value="<?= $_REQUEST['email'] ?>"/>
    <input type="hidden" name="PHPSESSID" value="<?= session_id() ?>" />
<div data-role="page">
		<div data-role="header" data-theme="a">
            <a href="http://<?= $_SERVER["HTTP_HOST"] ?>/vote/index.php" data-theme="e" data-role="button" data-icon="home" data-iconpos="notext" data-transition="fade" data-direction="reverse" class="ui-btn-left jqm-home">Menu</a>
            <h1><?= $txtTitel ?></h1>
		</div>
		<div data-role="content" data-theme="b">
			<div data-role="fieldcontain">

                <label for="fragender_name"><?= $txtName ?>:</label>
                <input type="text" name="fragender_name" id="fragender_name" style="background-color: #FFFFFF;<?= $error_feld_style_name ?>" value="<?= $fragender_name ?>"  />

                <label for="frage"><?= $txtFrage ?>:</label>
                <textarea id="frage" name="frage" style="background-color: #FFFFFF;"><?= $frage ?></textarea>
<?php if ($ok){ ?>
   	    		<a href="index.php?PHPSESSID=<?= session_id() ?>" data-theme="e" data-role="button" data-iconpos="left" data-icon="arrow-l"  data-transition="fade"><?= $txtBack ?></a>
<?php } else { ?>
       			<input type="submit" id="submit" name="submit" data-theme="b"  data-transition="fade" value="<?= $txtSend ?>"/>
<?php } ?>
 			</div>
        </div>
	<div data-role="footer" data-theme="a"  class="ui-bar">&nbsp;
    </div>
</div>
</form>
</body>
<?php
if ($error_feld_style_frage!=""){
?>
<script language="JavaScript" type="text/javascript">
    document.getElementById("frage").focus();
</script>
<?php
}
if ($error_feld_style_email!=""){
?>
<script language="JavaScript" type="text/javascript">
    document.getElementById("email").focus();
</script>
<?php
}
if ($error_feld_style_name!=""){
?>
<script language="JavaScript" type="text/javascript">
    document.getElementById("fragender_name").focus();
</script>
<?php
}
?>
</html>
