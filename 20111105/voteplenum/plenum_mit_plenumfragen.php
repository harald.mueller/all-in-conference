<?php
include("../voteumgebung/functions.php");
ConnectDB();
?>
<html>
	<head>
	<title>All in Conference | Plenum</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="shortcut icon" href="../voteumgebung/images/aic-logo.ico" />
  <link rel="apple-touch-icon" href="../voteumgebung/images/aic-logo-touch.jpg" />
	<script type="text/javascript" src="../voteumgebung/jquery/jquery-1.6.3.min.js"></script>
</head>
<body>
<form name="frm_Eingabe">
    <table>
        <tr>
            <td style="padding-left:10px;">
<?php
    $sql_listing = "SELECT * FROM `".$_SESSION["db_name"]."`.`adhoc_fragen` WHERE fuersplenum='1' AND gueltig='1'";
    $result = mysql_query($sql_listing);
    if (!$result) {
        echo "DB-Anfrage ist schief gegangen";
        exit;
    }
    if (mysql_num_rows($result) == 0) {
        echo "Keine Eintraege";
        exit;
    }
    $boxenBreite = "700";
    $erkennungsBild = "../voteumgebung/images/leer.gif";
	$zaehler=0;
	while ($row = mysql_fetch_assoc($result)) {
	    $zaehler++;
?>
            <img src='../voteumgebung/images/leer.gif' width='<?= $boxenBreite ?>' height='20'>
            <table border='0' cellspacing='0' summary='' cellpadding='0'>
                <tr>
                  <td>
                    <table border='0' cellspacing='0' summary='' cellpadding='0' width='<?= $boxenBreite ?>'>
                        <tr>
                          <td height='28' valign='top' width='50' align='left'><img alt='' src='../voteumgebung/layout/coolness2blau.gif'></td>
                          <td height='28' valign='middle' width='100%' background='../voteumgebung/layout/coolness3.gif' >
                          	<table cellpadding="0" cellspacing="0" width="100%" height="100%">
                          		<tr>
	                              <td width="50" align="center" style='font-family: Arial,helvetica; font-size: 7pt'>-&nbsp;<?= $row['id'] ?>&nbsp;-
	                              </td>
	                          		<td width='200' style='font-family: Arial,helvetica; font-size: 7pt'><?= $row['fragender_name'] ?>
	                              </td>
	                              <td style='font-family: Arial,helvetica; font-size: 7pt'><?= $row['fragender_email'] ?>
	                              </td>
	                              <td align="right" style='font-family: Arial,helvetica; font-size: 6pt'><?= $row['timestamp'] ?>
	                              </td>
	                          	</tr>
	                          </table>
	                        <td height='28' valign='middle' width='50' align='left'><img src='../voteumgebung/layout/coolness1.gif' border="0"></td>
                        </tr>
                    </table>
                    <table border='0' cellspacing='0' summary='' cellpadding='0' width='<?= $boxenBreite ?>'>
                        <tr valign='top'>
                          <td valign='top' background='../voteumgebung/layout/coolness6.gif' width='4' align='left'><img alt='' src='../voteumgebung/layout/coolness5.gif' border="0"></td>
                          <td bgcolor='white' valign='top' align='left' width="100%" style='font-family: Arial,helvetica; font-size: 18pt;padding-left: 6px;color: #000000'><?= $row['frage'] ?></td>
                          <td valign='top' background='../voteumgebung/layout/coolness4.gif' width='4' align='right'><img alt='' src='../voteumgebung/layout/coolness5.gif'></td>
                        </tr>
                    </table>
                    <table border='0' cellspacing='0' summary='' cellpadding='0' width='<?= $boxenBreite ?>'>
                        <tr>
                          <td background='../voteumgebung/layout/coolness8.gif' width='100%'><img alt='' src='../voteumgebung/layout/coolness9.gif' height="10"></td>
                          <td background='../voteumgebung/layout/coolness8.gif'><img alt='' src='../voteumgebung/layout/coolness7.gif' height="10"></td>
                        </tr>
                    </table>
                  </td>
                </tr>
            </table>
<?php
    }//end while
?>
            </td>
        </tr>
    </table>
</form>

</body>
</html>
