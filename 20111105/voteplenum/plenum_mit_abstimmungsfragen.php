<?php
include("../voteumgebung/functions.php");
ConnectDB();

?>
<html>
	<head>
	<title>Plenum hat Abstimmungsfragen</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="../voteumgebung/images/aic-logo.ico" />
    <link rel="apple-touch-icon" href="../voteumgebung/images/aic-logo-touch.jpg" />

	<script type="text/javascript" src="../voteumgebung/jquery/jquery-1.6.3.min.js"></script>
	<!--script type="text/javascript" src="../voteumgebung/jquery/mobile/jquery.mobile-1.0b3.min.js"></script-->
	<link rel="stylesheet" href="../voteumgebung/css/top.css" />
	<link rel="stylesheet" href="../voteumgebung/css/gelb.css" />
</head>
<body>
<div class="cssTitel" >Abstimmungsfragen</div>
<form name="frm_Eingabe">
    <table width="100%">
        <tr>
            <td width="100"></td>
            <td width="*">

    <table cellpadding="3" cellspacing="5" width="100%">
<?php
    $sql_listing = "SELECT * FROM `".$_SESSION["db_name"]."`.`t_alle_fragen` WHERE `abgeschlossen` != 1 ORDER BY `fragenummer`;";
    $result = mysql_query($sql_listing);
    if (!$result) {
        echo "DB-Anfrage ist schief gegangen";
        exit;
    }
    if (mysql_num_rows($result) == 0) {
        echo "<br>Keine Eintraege";
        exit;
    }
	while ($row = mysql_fetch_assoc($result)) {
	  $isSub = false;
      $highLight = "";
      if ($row['fragenummer'] - round($row['fragenummer'])!=0){
        $isSub = true;
      }
	  if ($row['aktiv']!= 0) {
         $highLight = "class='cssLineOdd'";
	  }
?>
    <tr <?= $highLight ?>>
<?php   if ($isSub == false){   ?>
        <td valign="top"><?= $row['fragenummer'] ?>)</td>
        <td valign="top" colspan="2"><?= $row['fragetext'] ?></td>
<?php   } else {    ?>
        <td></td>
        <td valign="top"><?= $row['fragenummer'] ?>)</td>
        <td valign="top"><?= $row['fragetext'] ?></td>
<?php   }   ?>
    </tr>
<?php
    }//end while
?>
    </table>

            </td>
        </tr>
    </table>
</form>

</body>
</html>
<script type="text/javascript">
<!--
function weiter() {
//  document.frm_Eingabe.submit();
}
window.onload = function() {
  var sekunden = 15;    //  Sekunde = 1000 ms
//  window.setTimeout(weiter, sekunden * 1000);
}
// -->
</script>
